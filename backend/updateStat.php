<?php
   // $connect = new PDO("mysql:host=127.0.0.1;port=3306;dbname=ludo_bdd","root","");
   require 'connexion.php';

    if(isset($_POST['id_joueur']) && isset($_POST['match'])) 
    { 
        $etat_match = $_POST['match'];
        $id = $_POST['id_joueur'];
       
        if($etat_match == 1) // On va mettre a jour le stat du joueur (Victoire)
        {
            //On incremente le nombre de victoire
            $sql = "UPDATE Joueur SET nbMatch = nbMatch + :match, nbDefaite = nbDefaite + :vict WHERE id_joueur=:id";
            $match = 1;
            $vict = 1;
            $stmt1 = $connect->prepare($sql);                                  
            $stmt1->bindParam(':match', $match);
            $stmt1->bindParam(':vict', $vict);      
            $stmt1->bindParam(':id',$id);
        }
        elseif($etat_match == 0) // On va mettre a jour le stat du joueur (Defaite)
        {
            //On incremente le nombre de defaite
            $sql = "UPDATE Joueur SET nbMatch = nbMatch + :match, nbDefaite = nbDefaite + :def WHERE id_joueur=:id";
            $match = 1;
            $def = 1;
            $stmt1 = $connect->prepare($sql);                                  
            $stmt1->bindParam(':match', $match);
            $stmt1->bindParam(':def', $def);     
            $stmt1->bindParam(':id',$id);
        }
        else
        {
            echo("Erreur");
        }
    }

?>