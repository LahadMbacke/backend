<?php 
    // $connect = new PDO("mysql:host=127.0.0.1;port=3306;dbname=ludo_bdd","root","");
    require 'connexion.php';

     if(isset($_POST['id_joueur']) && isset($_POST['id_match']) && isset($_POST['rang']) ) 
     {
        $id_joueur = $_POST['id_joueur'];
        $id_match = $_POST['id_match'];
        $rang = $_POST['rang'];

        $req = "INSERT INTO Jouer (id_joueur,id_partie,rang) VALUES(?,?,?)";
         $stmt = $connect->prepare($req);           
         $stmt->bindParam(1, $id_joueur);
         $stmt->bindParam(2, $id_match);
         $stmt->bindParam(3, $rang);
         $stmt->execute();

         if ($stmt) 
            echo "1"; // Pour dire que l'insertion a reuissi
         else 
            echo "0"; // Echec de l'insertion
     }

?>