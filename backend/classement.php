<?php
       // $connect = new PDO("mysql:host=127.0.0.1;port=3306;dbname=ludo_bdd","root","");
       require 'connexion.php';

    if(isset($_POST['clasmnt'])) 
    { 
       // On fait un select pour trouver les stas des joueur , on les classe par nbVict et nbDefaite
        $req = "SELECT * FROM Statistique S join Joueur J ON J.idJoueur = S.idJoueur
         Order by nbVictoire DESC , nbDefaite ASC";
         $res = $connect->prepare($req);
        
        $retVal = array("response" => "Echec"); // On declare un tableau 
        $rang = 1;  // la postion d'un joueur dans le classement
        while ($row = $res->fetch(PDO::FETCH_ASSOC)) { //on parcour chaque ligne corespondant a un joueur
            //On affiche ses infos
           //On met les infos du joueur sur le tableau
           $retVal = array("Rang" => $rang,
           "Pseudo" => $row["pseudo"],
           "nbVictoire" => $row["nbVictoire"],
           "nbDefaite" => $row["nbDefaite"],
           "response" => "Ok");
           $rang++;

           echo json_encode($retVal); // On affiche le classement des joueur et les stat
        }

        
    }

?>